from setuptools import setup

setup(
    name='sedbgmux',
    version='1.0',
    url='https://gitea.osmocom.org/fixeria/sedbgmux',
    license='GPLv3',
    author='Vadim Yanitskiy',
    author_email='fixeria@osmocom.org',
    description='DebugMux client for [Sony] Ericsson phones and modems',
    packages=['sedbgmux', 'sedbgmux.io', 'sedbgmux.ch'],
    install_requires=[
        'construct',
        'pyserial',
        'crcmod',
        'cmd2 >= 2.0.0',
    ],
    extras_require={
        # sedbgmux-dump.py optionally requires pyshark
        'btpcap' : ['pyshark'],
    },
    scripts=[
        'sedbgmux-shell.py',
        'sedbgmux-dump.py',
    ]
)
