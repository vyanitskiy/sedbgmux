# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2023  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging

from . import DumpIO
from . import DumpIOError
from . import DumpIOEndOfFile

# local logger for this module
log = logging.getLogger(__name__)


class DumpIOBtPcap(DumpIO):
    ''' Read-only interface for parsing Bluetooth RFCOMM captures '''

    RFCOMM_CHAN: int = 2
    ARGS = {
        'chan' : 'RFCOMM channel number (default: 2)',
         }

    def __init__(self, fname: str, readonly: bool = True, **kw) -> None:
        self.buf = bytearray()
        self.dir = ''  # cached
        self.timestamp = 0.0  # cached
        self.readonly = readonly

        if 'chan' in kw:
            chan = int(kw['chan'])
        else:
            chan = self.RFCOMM_CHAN

        try:
            from pyshark import FileCapture
        except ImportError as e:
            raise DumpIOError('pyshark is not installed') from e

        log.info('Opening Bluetooth RFCOMM capture %s', fname)
        dfilter = 'btrfcomm.channel == %s' % chan
        self._pcap = FileCapture(fname, display_filter=dfilter)
        self._find_connect()

    def read(self) -> dict:
        ''' Read a single record from dump '''
        frame: bytes = self._read(4)  # Magic + Length
        if frame[:2] != b'\x42\x42':
            raise DumpIOError('Unexpected DebugMux frame magic')
        (dir, timestamp) = (self.dir, self.timestamp)
        length: int = int.from_bytes(frame[2:], byteorder='little')
        frame += self._read(length)
        return dict(timestamp=timestamp, dir=dir, data=frame)

    def write(self, record: dict) -> None:
        ''' Store a single record to dump (no-op method, read-only) '''
        if self.readonly:
            raise DumpIOError('Read-only mode')
        raise NotImplementedError

    def _parse_chunk(self) -> bytes:
        ''' Parse an Rx/Tx chunk from the given PCAP file '''
        while True:
            try:
                pkt = self._pcap.next()
            except StopIteration:
                raise DumpIOEndOfFile
            if int(pkt.btrfcomm.len) > 0:
                break
        log.debug('PCAP frame #%05u (time %s, len %s / plen %s): %s',
                  int(pkt.number), pkt.sniff_time,
                  pkt.length, pkt.btrfcomm.len,
                  pkt.DATA.data)
        self.dir = 'Tx' if pkt.bthci_acl.src_role == '1' else 'Rx'
        self.timestamp = pkt.sniff_time.timestamp()
        return bytes.fromhex(pkt.DATA.data)

    def _find_connect(self) -> None:
        ''' Skip DebugMux carrier establishment (AT commands) '''
        while True:
            data: bytes = self._parse_chunk()
            if self.dir != 'Rx':
                continue
            if data.strip() == b'CONNECT':
                break
        log.debug('Found the CONNECT line')

    def _read(self, length: int) -> bytes:
        ''' Read the given number of bytes from the buffer '''
        while len(self.buf) < length:
            data: bytes = self._parse_chunk()
            self.buf.extend(data)
        data = bytes(self.buf[:length])
        self.buf = self.buf[length:]
        return data
