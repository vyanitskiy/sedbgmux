# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2022  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import sys

from . import DbgMuxConnHandler
from . import DbgMuxConnState

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxConnTerminal(DbgMuxConnHandler):
    ''' Terminal for communicating with 'Interactive Debug' DPs '''

    def __init__(self):
        super().__init__()

        self.attached: bool = False

    def attach(self) -> None:
        ''' Read data blocks from stdin and enqueue to the Tx queue '''
        self.attached = True
        while self._connected.is_set():
            try:
                line: str = input()
                if self.conn_state == DbgMuxConnState.Established:
                    self.send_data(bytes(line + '\r', 'ascii'))
            except (KeyboardInterrupt, EOFError) as e:
                break
        self.attached = False

    def _conn_data(self, data: bytes) -> None:
        if self.attached:
            sys.stdout.write(data.decode('ascii'))
            sys.stdout.flush()
