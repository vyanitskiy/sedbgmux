# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2023  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import typing

from . import DbgMuxConnHandler

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxConnFileLogger(DbgMuxConnHandler):
    ''' Log all received connection data to a file (binary mode) '''

    def __init__(self, file: typing.BinaryIO):
        super().__init__()

        self._file = file

    def _conn_data(self, data: bytes) -> None:
        ''' Called on receipt of a data block '''
        self._file.write(data)
