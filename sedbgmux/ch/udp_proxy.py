# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2022-2023  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import threading
import socket

from typing import Tuple

from . import DbgMuxConnHandler
from . import DbgMuxConnState

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxConnUdpProxy(DbgMuxConnHandler):
    ''' Expose a DebugMux connection as a UDP socket '''

    LADDR_DEF: Tuple[str, int] = ('127.0.0.1', 2424)
    RADDR_DEF: Tuple[str, int] = ('127.0.0.1', 4242)
    DGRAM_TIMEOUT_DEF: float = 0.2
    DGRAM_MAX_LEN: int = 2048

    def __init__(self, *args, **kw):
        super().__init__(*args)

        self.laddr: Tuple[str, int] = kw.get('laddr', self.LADDR_DEF)
        self.raddr: Tuple[str, int] = kw.get('raddr', self.RADDR_DEF)

        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.bind(self.laddr)
        self._sock.settimeout(self.DGRAM_TIMEOUT_DEF)

        self._tx_thread = threading.Thread(target=self._tx_worker,
                                           daemon=True)

    def _tx_worker(self) -> None:
        ''' Receive data blocks as datagrams and enqueue to the Tx queue '''
        while self._connected.is_set():
            try:
                (data, addr) = self._sock.recvfrom(self.DGRAM_MAX_LEN)
                if self.conn_state == DbgMuxConnState.Established:
                    self.send_data(data)
            except TimeoutError:
                pass
        log.debug('Thread \'%s-Tx\' is shutting down', self.__class__.__name__)

    def _conn_established(self) -> None:
        ''' Called when a connection has been established '''
        self._tx_thread.start()

    def _conn_terminated(self) -> None:
        ''' Called when a connection has been terminated '''
        self._tx_thread.join()

    def _conn_data(self, data: bytes) -> None:
        ''' Called on receipt of a data block '''
        self._sock.sendto(data, self.raddr)
