# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2022-2023  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import threading
import queue

from typing import Dict, Tuple
from construct import Container

from . import DbgMuxFrame
from . import DbgMuxPeer
from . import DbgMuxPingPong

from .ch import DbgMuxConnHandler
from .ch import DbgMuxConnState

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxClient:
    ''' DebugMux client role implementation '''

    IDENT_TIMEOUT: float = 0.5
    CONN_EST_TIMEOUT: float = 1.0
    CONN_TERM_TIMEOUT: float = 1.0

    data_providers: Dict[int, str]  # { DPRef : Name }
    pending_conn: Dict[int, DbgMuxConnHandler]  # { DPRef : ch }
    active_conn: Dict[int, Tuple[int, DbgMuxConnHandler]]  # { ConnRef : (DPRef, ch) }

    def __init__(self, peer: DbgMuxPeer):
        self.target_name: str = ''
        self.target_imei: str = ''
        self.data_providers = dict()
        self.pending_conn = dict()
        self.active_conn = dict()

        self.peer: DbgMuxPeer = peer
        self.ping_pong = DbgMuxPingPong(peer)

        self._rx_thread = threading.Thread(target=self._rx_worker,
                                           name='DbgMuxClient-Rx',
                                           daemon=True)
        self._shutdown = threading.Event()

        self._ev_ident = threading.Event()
        self._ev_conn_est = threading.Event()
        self._ev_conn_term = threading.Event()

    def start(self) -> None:
        ''' Start dequeueing messages from peer '''
        self._shutdown.clear()
        self._rx_thread.start()

    def stop(self) -> None:
        ''' Stop dequeueing messages from peer '''
        self._shutdown.set()
        self._rx_thread.join()
        self._reset()

    def _reset(self) -> None:
        ''' Reset the internal state '''
        self.target_name = ''
        self.target_imei = ''

        for (_, ch) in self.active_conn.values():
            ch._handle_terminated()
            del ch
        self.active_conn.clear()
        self.pending_conn.clear()
        self.data_providers.clear()

        self._ev_ident.clear()
        self._ev_conn_est.clear()
        self._ev_conn_term.clear()

    def enquiry(self) -> bool:
        ''' Enquiry target identifier and available Data Providers '''
        self._ev_ident.clear()
        self.peer.send(DbgMuxFrame.MsgType.Enquiry)
        if not self._ev_ident.wait(timeout=self.IDENT_TIMEOUT):
            log.error('Timeout waiting for identification response')
            return False
        return True

    def ping(self, payload: str = 'ping') -> bool:
        ''' Send a Ping to the target, expect Pong '''
        return self.ping_pong.ping(payload)

    def conn_establish(self, DPRef: int, ch: DbgMuxConnHandler) -> bool:
        ''' Establish connection with a Data Provider '''
        if DPRef not in self.data_providers:
            log.error('DPRef=0x%04x is not registered', DPRef)
            return False
        if DPRef in self.pending_conn:
            log.error('DPRef=0x%04x already has a pending connection', DPRef)
            return False
        for conn in self.active_conn.values():
            if conn[0] != DPRef:
                continue
            log.error('DPRef=0x%04x already has an active connection', DPRef)
            return False
        self._ev_conn_est.clear()
        self.pending_conn[DPRef] = ch
        ch.establish(DPRef, self.peer._tx_queue)
        if not self._ev_conn_est.wait(timeout=self.CONN_EST_TIMEOUT):
            log.error('Timeout establishing connection with DPRef=0x%04x', DPRef)
            self.pending_conn.pop(DPRef)
            return False
        return ch.conn_state == DbgMuxConnState.Established

    def conn_terminate(self, ConnRef: int) -> bool:
        ''' Terminate connection with a Data Provider '''
        if ConnRef not in self.active_conn:
            log.error('ConnRef=0x%04x is not registered', ConnRef)
            return False
        self._ev_conn_term.clear()
        (DPRef, ch) = self.active_conn.get(ConnRef)
        ch.terminate()
        if not self._ev_conn_term.wait(timeout=self.CONN_TERM_TIMEOUT):
            log.error('Timeout terminating connection with DPRef=0x%04x', DPRef)
            return False
        return ch.conn_state == DbgMuxConnState.NotEstablished

    def conn_send_data(self, ConnRef: int, data: bytes) -> bool:
        ''' Send some data to a Data Provider '''
        if ConnRef not in self.active_conn:
            log.error('ConnRef=0x%04x is not registered', ConnRef)
            return False
        (DPRef, ch) = self.active_conn.get(ConnRef)
        if ch.conn_state != DbgMuxConnState.Established:
            log.error('ConnRef=0x%04x is not established (yet?)', ConnRef)
            return False
        ch.send_data(data)
        return True

    def _rx_worker(self) -> None:
        ''' Dequeue DebugMux frames from peer and handle them '''
        while not self._shutdown.is_set():
            try:
                frame: Container = self.peer._rx_queue.get(block=True, timeout=0.5)
                self._handle_frame(frame)
                self.peer._rx_queue.task_done()
            except queue.Empty:
                pass
        log.debug('Thread \'%s\' is shutting down', threading.current_thread().name)

    def _handle_frame(self, frame: Container) -> None:
        MsgType, Msg = frame['MsgType'], frame['Msg']
        if MsgType == DbgMuxFrame.MsgType.Ident:
            self._handle_msg_ident(Msg)
            self._ev_ident.set()
        elif MsgType == DbgMuxFrame.MsgType.DPAnnounce:
            self._handle_msg_dp_announce(Msg)
        elif MsgType == DbgMuxFrame.MsgType.Pong:
            self.ping_pong._handle_pong(Msg['Data'])
        elif MsgType == DbgMuxFrame.MsgType.ConnEstablished:
            self._handle_msg_conn_est(Msg)
            self._ev_conn_est.set()
        elif MsgType == DbgMuxFrame.MsgType.ConnTerminated:
            self._handle_msg_conn_term(Msg)
            self._ev_conn_term.set()
        elif MsgType == DbgMuxFrame.MsgType.ConnData:
            self._handle_msg_conn_data(Msg)
        elif MsgType == DbgMuxFrame.MsgType.FlowControl:
            self._handle_msg_flow_control(Msg)
            return  # no Ack
        elif MsgType == DbgMuxFrame.MsgType.Ack:
            return  # no Ack
        else:
            log.warning('Unhandled DebugMux message %s: %s', MsgType, Msg)
        self.peer.send(DbgMuxFrame.MsgType.Ack)

    def _handle_msg_ident(self, msg: Container) -> None:
        ''' Handle DbgMuxFrame.MsgType.Ident '''
        self.target_name = msg['Ident'][:-15]
        self.target_imei = msg['Ident'][-15:]
        log.info('Identified target: \'%s\', IMEI=%s',
                 self.target_name, self.target_imei)

    def _handle_msg_dp_announce(self, msg: Container) -> None:
        ''' Handle DbgMuxFrame.MsgType.DPAnnounce '''
        DPRef, Name = msg['DPRef'], msg['Name']
        log.info('Data Provider available (DPRef=0x%04x): \'%s\'', DPRef, Name)
        if DPRef in self.data_providers:
            log.warning('DPRef=0x%04x was already announced', DPRef)
        self.data_providers[DPRef] = Name

    def _handle_msg_conn_est(self, msg: Container) -> None:
        ''' Handle DbgMuxFrame.MsgType.ConnEstablished '''
        DPRef, ConnRef = msg['DPRef'], msg['ConnRef']
        log.info('Rx ConnEstablished: ConnRef=0x%04x, DPRef=0x%04x', ConnRef, DPRef)
        if ConnRef == 0xffff:
            log.warning('Connection establishment failed: '
                        'no such DPRef=0x%04x?', DPRef)
            return
        elif ConnRef in self.active_conn:
            log.error('ConnRef=0x%04x is already established', ConnRef)
            return
        elif DPRef not in self.data_providers:
            log.error('DPRef=0x%04x is not registered', DPRef)
            return
        elif DPRef not in self.pending_conn:
            log.error('DPRef=0x%04x has no pending connection', DPRef)
            return
        ch = self.pending_conn.pop(DPRef)
        self.active_conn[ConnRef] = (DPRef, ch)
        ch._handle_established(ConnRef, msg['DataBlockLimit'])

    def _handle_msg_conn_term(self, msg: Container) -> None:
        ''' Handle DbgMuxFrame.MsgType.ConnTerminated '''
        DPRef, ConnRef = msg['DPRef'], msg['ConnRef']
        log.info('Rx ConnTerminated: ConnRef=0x%04x, DPRef=0x%04x', ConnRef, DPRef)
        if ConnRef == 0xffff:
            log.warning('Connection termination failed: '
                        'no such DPRef=0x%04x?', DPRef)
            return
        elif ConnRef not in self.active_conn:
            log.error('ConnRef=0x%04x is not established', ConnRef)
            return
        elif DPRef not in self.data_providers:
            log.error('DPRef=0x%04x is not registered', DPRef)
            return
        # Old DPRef becomes invalid, remove it
        del self.data_providers[DPRef]
        (_, ch) = self.active_conn.pop(ConnRef)
        ch._handle_terminated()

    def _handle_msg_conn_data(self, msg: Container) -> None:
        ''' Handle DbgMuxFrame.MsgType.ConnData '''
        ConnRef, Data = msg['ConnRef'], msg['Data']
        if ConnRef not in self.active_conn:
            log.error('ConnRef=0x%04x is not established', ConnRef)
            return
        (DPRef, ch) = self.active_conn.get(ConnRef)
        ch._handle_data(Data)

    def _handle_msg_flow_control(self, msg: Container) -> None:
        ''' Handle DbgMuxFrame.MsgType.FlowControl '''
        ConnRef, DataBlockCount = msg['ConnRef'], msg['DataBlockCount']
        if ConnRef not in self.active_conn:
            log.error('ConnRef=0x%04x is not established', ConnRef)
            return
        (DPRef, ch) = self.active_conn.get(ConnRef)
        ch._handle_flow_control(DataBlockCount)
