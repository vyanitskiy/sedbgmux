# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2022-2023  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import threading

from . import DbgMuxFrame
from . import DbgMuxPeer

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxPingPong:
    ''' Implements link testing logic '''

    def __init__(self, peer: DbgMuxPeer, timeout: float = 1.0):
        self.peer: DbgMuxPeer = peer
        self.timeout: float = timeout
        self._pong = threading.Event()
        self._expect_pong: bool = False

    def ping(self, payload: str) -> bool:
        log.info('Tx Ping with payload \'%s\'', payload)
        self._pong.clear()
        self._expect_pong = True
        self.peer.send(DbgMuxFrame.MsgType.Ping, dict(Data=payload))
        if not self._pong.wait(timeout=self.timeout):
            log.warning('Timeout waiting for Pong')
            return False
        return True

    def _handle_pong(self, payload: str) -> None:
        if not self._expect_pong:
            log.warning('Rx unexpected Pong, sending ACK anyway')
            return
        log.info('Rx Pong with payload \'%s\'', payload)
        self._expect_pong = False
        self._pong.set()
